from collections import Counter

"""
Hey Dan,

I'm having some trouble figuring out
how to make the class aspect work...
How would I take the functions below and
put them in a class?
"""

#Use function add() to take in objects and sort by frequency.
def add():
	#Ask the user to input objects.
	string_input = raw_input("Input a list of objects separated by spaces: ")
	#Split the objects by spaces to count frequency.
	input_list = string_input.split() 
	#Counter().most_common() will sort the list of objects by frequency.
	counter = Counter(input_list).most_common()
	#Finally, we return the sorted list.
	return counter

#Use function get_ordered_list() to give the output we want.
def get_ordered_list():
	#Define an empty list that we will fill with values in a moment.
	ordered_list = []
	#Define variable c as returned value from our add() function.
	c = add()
	#Loop through the values to print out just the objects
	#ordered by frequency on the same line.
	for letter, count in c:
		ordered_list.append('%s' % (letter))
	print ', '.join(ordered_list)

#Run the function.
get_ordered_list()

