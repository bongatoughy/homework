import requests
from bs4 import BeautifulSoup

#Ask the user for the URL to get the links from.
url = raw_input("Copy and paste a url to see the links of the page: ")

def find_a(url):
	#Use REQUESTS to get the url (variable defined above).
	r = requests.get(url)
	#Use BEAUTIFUL SOUP to get the content of the url
	soup = BeautifulSoup(r.content)
	#The variable links will store all of the links...
	links = soup.find_all("a")
	#by looping through them.
	for link in links:
		#Finally, print the links.
		print "<a href='%s'>%s</a>" %(link.get("href"), link.text)

#Exception Handling
try:
	find_a(url)
except:
	print "Hmm, there was a problem. Did you copy and paste the url?"
